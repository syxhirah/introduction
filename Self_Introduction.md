# **Self Introduction**

## About me
![me](https://media-exp1.licdn.com/dms/image/C4E03AQFW_3qFNCeqig/profile-displayphoto-shrink_200_200/0/1603717751631?e=1640822400&v=beta&t=rt-eYnr3NUzSsBHcjmqGozmBSRG5Dm5Z0nRVuXs60U4)

### Name: Nur Syahirah binti Shafek Hamlan
### Age : 24 years old
### Location: Sepang, Selangor, Malaysia

| Strengths | Weaknesses |
| ----------- | ----------- |
| Focus | Impatient |
| Flexible | Insecure |
| Creative | Lack of confidence |

### Fun fact about meeeeeee
- I can swim 
- I can speak Japanese
- I enjoy backpacking during travelling
