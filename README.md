### Table of Contents

------

#### Introduction

#### Weekly Log

- [Week 1](https://gitlab.com/syxhirah/introduction/-/blob/main/Week_1.md)
- [Week 2](https://gitlab.com/syxhirah/introduction/-/blob/main/Week_2.md)
- [Week 3](https://gitlab.com/syxhirah/introduction/-/blob/main/Week_3.md)
- [Week 4](https://gitlab.com/syxhirah/introduction/-/blob/main/Week_4.md)
- [Week 5](https://gitlab.com/syxhirah/introduction/-/blob/main/Week_5.md)
- [Week 6](https://gitlab.com/syxhirah/introduction/-/blob/main/Week_6.md)
- Week 7
- Week 8
- Week 9
- Week 10
- Week 11
- Week 12
- Week 13
- Week 14

#### Results and Findings

#### Conclusion

#### Reflection



