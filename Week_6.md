## Integrated Design Project (IDP) - Week 6 Log 

Name: Nur Syahirah binti Shafek Hamlan

Student number: 197450

Subsystem: Flight System Integration 

Team: 7

------

#### Activity 1 - Propulsion Equipment Testing (Wednesday)

- Tested the functionality and performance of ESC and battery.
- The ESC was not functioning so it was replaced with a new one.
- It is important to make sure that the propeller should be placed on the correct side to obtain correct thrust reading. 
- Other parts of the airship will be tested near future. 

