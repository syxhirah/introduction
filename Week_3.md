## Integrated Design Project (IDP) - Week 3 Log 

Name: Nur Syahirah binti Shafek Hamlan

Student number: 197450

Subsystem: Flight System Integration 

Team: 7

------

#### Activity 1 - Thrust Test  (Wednesday)

- Assisted the Propulsion subsystem in learning how to setup a static thrust test at the propulsion lab. 
- Performed calibration using RCbenchmark software.

#### Activity 2 - Team meeting for current updates (Friday)

Attendance: All present except 

Power and Propulsion 

 - Performed static thrust test.
 - Another new test using new motor.
 - Identified a suitable propeller for the motor.

Design

- Gondola design ideas - 3 layers, esc, motor, distributed load cover.
- Designed gondola should be able carry at max 20kg payload.

Simulation

- Getting a measurement and dimensions of the airship.
- Tasked to draw the airship using Solidworks.

Software and Control

- Configuring the PID design controller for the vehicle to develop EOMs for longitudinal and lateral motions along with its Euler Angular Rates, roll, pitch, yaw. 

- Researching the overall vehicle attitude control system from Earth Frame Controller to Body Frame Controller using Simulink.
- Testing the vehicle using the UAV Monitor Software using the Putra Space simulation to evaluate the rotational speed for each motor.

#### Activity 3 - Finalization and Presentation of Gantt Chart (Friday)

- The Gantt Chart for Flight System Integration was completed this week and presented to Dr. Salahuddin on Friday. 

