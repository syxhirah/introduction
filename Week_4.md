## Integrated Design Project (IDP) - Week 4 Log 

Name: Nur Syahirah binti Shafek Hamlan

Student number: 197450

Subsystem: Flight System Integration 

Team: 7

------

#### Activity 1 - Assisting The Propulsion Subsystem (Tuesday)

- Helped the propulsion team in finding the drag of the airship by using a trolley.
- It is the easiest and most low cost method to find its drag.
- Unlike other methods such as filling the airship using helium gas cost a lot more.

#### Activity 2 - Subsystem and Team Meeting (Friday)

- Making sure that the project is on-track and well up-to-date.
- Resurfaced problems related to the project and discussed about solutions and approaches to solve them.
- New solutions were found to make more systematic attendance and roles recording using excel spreadsheet.

