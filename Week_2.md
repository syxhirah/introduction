## Integrated Design Project (IDP) - Week 2 Log 

Name: Nur Syahirah binti Shafek Hamlan

Student number: 197450

Subsystem: Flight System Integration 

Team: 7

------

#### Activity 1 - Inventory Check (Monday, 24/10/2021, 10AM)

- The status and condition every component (such as main structure, propellers and control systems) of the LTA aircraft were identified; whether they need to be repaired/replaced with a new one/still can be used.  Everything was updated in a checklist.

- After identifying items that need to be replaced with new ones, a list of bill of materials (BOM) was created for and presented to Dr. Salahuddin before finalizing and ordering from the supplier.

- Google Spreadsheet containing BoM and inventory list >> [Click here](https://docs.google.com/spreadsheets/d/1ij4VMTHH0KphTnuX29gXc1W_kzoVliqFd0tuFXs60F4/edit#gid=80781328)

#### Activity 2 - Gantt Chart (Friday, 30/10/2021, 3PM)

- The flight system integration subsystem was instructed to create a Gantt Chart to make the project more organized and to set a milestone.
- A completed list of tasks were inserted in the Gantt Chart. However, the dates and milestones were not finalized yet. 
- Google Spreadsheet containing the Gantt Chart >> [Click here](https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit)

#### Activity 3 - Team Meeting (Saturday, 31/10/2021, 3PM)

Attendance: All present

Agenda: 

 - Writing of logbook (formatting and content) 
 - Ice breaking - getting to know every team member and the subsystems they're working on.
 - Everyone was given a chance to tell the whole team about what are they working on their subsystems.

#### Activity 4 - Subsystem Meeting (Friday, 30/10/2021, 4PM)

Attendance: All present

Agenda: 

- General discussion and updates about the project.
- Strategize to help other subsystems in hitting their goals and milestones.

#### Activity 5 - Gitlab Crash Course (Tuesday, 25/10/2021, 9-10PM)	

- Mr. Azizi Malek was in charge to guide us on how to use Gitlab from A to Z, including installing and first-time setup.
- Practices and tasks were given to everyone to improve the understanding on Gitlab.
