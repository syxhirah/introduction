## Integrated Design Project (IDP) - Week 5 Log 

Name: Nur Syahirah binti Shafek Hamlan

Student number: 197450

Subsystem: Flight System Integration 

Team: 7

------

#### Activity 1 - Setting up parameters

- The amount of parameters needed for calculations are not enough. 
- Hence, to make the calculations more systematic and organized, an excel spreadsheet was created.
- This could also help other subsystem teams for reference and more info.

